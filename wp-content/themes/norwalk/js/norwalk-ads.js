
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})(); 

googletag.cmd.push(function() {
googletag.defineSlot('/1732998/Nancy_on_Norwalk_top_sidebar_large_rectangle_ad', [300, 250], 'div-gpt-ad-1390517935355-2').addService(googletag.pubads());
googletag.defineSlot('/1732998/Nancy-On-Norwalk-upper-sidebar', [300, 250], 'div-gpt-ad-1390517935355-0').addService(googletag.pubads());
googletag.defineSlot('/1732998/NancyOnNorwalk-middle-sidebar', [300, 250], 'div-gpt-ad-1390517935355-4').addService(googletag.pubads());
googletag.defineSlot('/1732998/NancyOnNorwalk-lower-sidebar', [300, 250], 'div-gpt-ad-1390517935355-3').addService(googletag.pubads());
googletag.defineSlot('/1732998/Nancy_on_Norwalk_bottom_sidebar_large_rectangle_ad', [300, 250], 'div-gpt-ad-1390517935355-1').addService(googletag.pubads());
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});

var p="http",d="static";if(document.location.protocol=="https:"){p+="s";d="engine";}var z=document.createElement("script");z.type="text/javascript";z.async=true;z.src=p+"://"+d+".adzerk.net/ados.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(z,s);

//Doug's old Ad-zerk code. Almost certainly obsolete now, kept in code just in case until it's confirmed.

// var ados = ados || {};
// ados.run = ados.run || [];
// ados.run.push(function() {
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Top*/
// ados_add_placement(8842, 58402, "azk58825", 5).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Top*/
// ados_add_placement(8842, 58402, "azk42297", 94).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Top*/
// ados_add_placement(8842, 58402, "azk16122", 5).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Top*/
// ados_add_placement(8842, 58402, "azk89194", 94).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Top*/
// ados_add_placement(8842, 58402, "azk41441", 5).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Top*/
// ados_add_placement(8842, 58402, "azk82570", 94).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Top*/
// ados_add_placement(8842, 58402, "azk11068", 5).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Top*/
// ados_add_placement(8842, 58402, "azk53963", 94).setZone(63065);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Middle*/
// ados_add_placement(8842, 58402, "azk63640", 5).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Middle*/
// ados_add_placement(8842, 58402, "azk5687", 94).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Middle*/
// ados_add_placement(8842, 58402, "azk88894", 5).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Middle*/
// ados_add_placement(8842, 58402, "azk68847", 94).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Middle*/
// ados_add_placement(8842, 58402, "azk50125", 5).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Middle*/
// ados_add_placement(8842, 58402, "azk55552", 94).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Middle*/
// ados_add_placement(8842, 58402, "azk93", 5).setZone(63066);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Middle*/
// ados_add_placement(8842, 58402, "azk16315", 94).setZone(63066);
//  /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Bottom */
// ados_add_placement(8842, 58402, "azk18489", 5).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk55410", 94).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk26093", 5).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk4238", 94).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk19150", 5).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk25616", 94).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk94823", 5).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Bottom*/
// ados_add_placement(8842, 58402, "azk2208", 94).setZone(63067);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk72251", 5).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk31557", 94).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk55755", 5).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk3273", 94).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk23262", 5).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk67876", 94).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x250 - Medium Rectangle, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk6796", 5).setZone(63068);
// /* load placement for account: imnct, site: NancyOnNorwalk, size: 300x150 - 300x150, zone: Tablet-Mobile*/
// ados_add_placement(8842, 58402, "azk11225", 94).setZone(63068);
// ados_load();
// });
 
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
qacct:"p-7GR1A7wZbYvVp"
});

